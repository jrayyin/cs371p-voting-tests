20

3
red
blue
orange
1 3 2
1 3 2
1 3 2
1 3 2
2 3 1
2 3 1
2 3 1
3 2 1
3 2 1

3
apple
orange
kiwi
1 3 2
1 3 2
1 3 2
1 3 2
2 3 1
2 3 1
2 3 1
3 2 1
3 2 1
3 2 1

2
rob
bob
1 2
2 1

5
laura
alex
janie
thomas
rachel
1 2 3 4 5
5 4 3 2 1
3 4 1 2 5
3 4 1 2 5
3 1 4 2 5
5 1 3 2 4
4 1 2 3 5
5 2 4 1 3
1 4 2 3 5

4
may
june
july
august
4 2 3 1
4 2 3 1
4 2 3 1
2 4 3 1
2 3 4 1
1 3 4 2
3 4 2 1
2 4 3 1
2 3 4 1
2 1 4 3

3
month
year
date
1 2 3
1 3 2
3 2 1
3 1 2
2 1 3
2 3 1
1 2 3
3 2 1
2 3 1

3
allie
addie
amy
1 2 3
1 2 3
1 3 2
3 1 2
2 1 3
3 2 1
2 3 1
1 3 2
2 3 1
2 3 1

4
spring
fall
summer
winter
3 2 1 4
1 3 2 4
1 3 2 4
1 3 2 4
1 2 4 3
3 1 4 2
3 1 2 4
4 2 3 1
3 1 4 2
4 2 1 3

5
fish 1
fish 2
fish 3
fish 4
fish 5
5 4 3 2 1
3 4 2 1 5
2 5 3 1 4

10
coffee
tea
smoothie
milkshake
ice cream
pastry
matcha
hot cocoa
boba
ayran
9 10 3 2 5 1 6 7 8 4

2
prokaryote
eukaryote
1 2
2 1
2 1
2 1
1 2

6
wood
metal
fire
earth
water
wind
2 4 5 1 3 6
6 2 5 3 4 1
2 5 1 6 4 3
1 3 5 6 4 2
1 2 3 5 6 4
1 3 2 5 6 4
1 6 3 5 2 4
1 6 3 2 4 5
1 3 4 2 5 6
6 1 4 3 2 5
1 4 6 5 2 3
1 4 5 3 2 6
1 3 2 5 4 6

3
leo
virgo
scorpio
1 2 3
3 2 1
2 3 1
3 2 1
3 1 2
2 3 1
2 3 1
1 3 2

4
physics
math
astronomy
computer science
4 3 1 2
2 4 1 3
2 3 4 1
4 3 2 1
3 4 2 1
2 1 4 3
3 2 1 4
4 3 2 1
2 3 4 1

1
one person
1
1
1
1
1

2
glasses
contacts
2 1
2 1
2 1
1 2
1 2

5
chess
checkers
mahjong
cards
tic-tac-toe
5 1 2 3 4
5 1 3 2 4
5 1 3 4 2
5 1 4 3 2
5 1 3 2 4
5 1 3 2 4
1 5 3 2 4
5 4 1 3 2

4
robin
sparrow
cardinal
eagle
1 2 3 4
3 4 2 1
1 2 3 4
1 3 2 4
1 3 2 4
4 3 1 2
2 3 4 1
2 3 4 1
3 4 2 1
4 3 1 2
3 4 1 2
4 3 1 2
2 3 4 1
3 4 1 2
3 4 1 2
3 4 1 2
3 4 1 2

2
morning
night
2 1
1 2
2 1
1 2
2 1
2 1
2 1

3
sneakers
boots
flip flops
3 1 2
3 1 2
1 2 3
1 3 2
3 1 2
3 1 2
3 1 2
3 1 2
